package eu.europeanainside.preview;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.preview.entities.PreviewTemplate;
import eu.europeanainside.preview.exception.InvalidTemplateException;
import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.exception.ResultNotFoundException;
import eu.europeanainside.preview.exception.TemplateNotFoundException;
import eu.europeanainside.preview.utils.ResultDirectoryUtils;
import org.junit.Assert;

/**
 * Tests for the PreviewServices class.
 *
 * @author inagy
 */
public class PreviewGeneratorTest {

  private static final String PLACEHOLDER = "placeholder";
  private static final String RECORD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    + "<catalog>"
    + "<cd>"
    + "<title>ÖÜÓŐÚÉÁŰÍöüóőúéáűí</title>"
    + "<artist>Bob Dylan</artist>"
    + "<country>USA</country>"
    + "<company>Columbia</company>"
    + "<price>10.90</price>"
    + "<year>1985</year></cd></catalog>";
  private static final String XSL = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    + "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
    + "<xsl:template match=\"/\">"
    + "<html>"
    + "<body>"
    + "<h2>My CD Collection</h2>"
    + "<table border=\"1\">"
    + "<tr bgcolor=\"#9acd32\">"
    + "<th>Title</th>"
    + "<th>Artist</th>"
    + "</tr>"
    + "<xsl:for-each select=\"catalog/cd\">"
    + "<tr>"
    + "<td><xsl:value-of select=\"title\"/></td>"
    + "<td><xsl:value-of select=\"artist\"/></td>"
    + "</tr>"
    + "</xsl:for-each>"
    + "</table>"
    + "</body>"
    + "</html>"
    + "</xsl:template>" + "</xsl:stylesheet>";
  private static final String EXPECTED = "<html>\n" + "   <body>\n"
    + "      <h2>My CD Collection</h2>\n" + "      <table border=\"1\">\n"
    + "         <tr bgcolor=\"#9acd32\">\n" + "            <th>Title</th>\n"
    + "            <th>Artist</th>\n" + "         </tr>\n"
    + "         <tr>\n" + "            <td>ÖÜÓŐÚÉÁŰÍöüóőúéáűí</td>\n"
    + "            <td>Bob Dylan</td>\n" + "         </tr>\n"
    + "      </table>\n" + "   </body>\n" + "</html>";
  private static final Logger LOGGER = LoggerFactory.getLogger(PreviewGeneratorTest.class);

  /**
   * Tests the ZIP bundle creation.
   *
   * @throws PreviewGenerationException Shouldn't happen.
   * @throws InvalidTemplateException Shouldn't happen.
   * @throws TemplateNotFoundException Shouldn't happen.
   */
  @Test
  public void testGeneratePreviewBundle() throws PreviewGenerationException,
    InvalidTemplateException, TemplateNotFoundException {
    PreviewGenerator generator = new TestablePreviewGenerator();
    byte[] result = generator.generatePreviewBundle(PLACEHOLDER,
      new PreviewTemplate(PLACEHOLDER, PLACEHOLDER));

    assertNotNull("Result is null.", result);
    assertFalse("Byte array is zero sized.", result.length == 0);

    ByteArrayInputStream inputStream = new ByteArrayInputStream(result);
    try {
      ZipInputStream zipInputStream = new ZipInputStream(inputStream);
      try {
        ZipEntry zipEntry = zipInputStream.getNextEntry();
        assertEquals("Zip name doesn't match.", zipEntry.getName(),
          PreviewGenerator.PREVIEW_HTML_NAME);

        Scanner fileScanner = new Scanner(new InputStreamReader(zipInputStream,
          "UTF-8"));
        fileScanner.useDelimiter("\\Z");
        assertTrue("File is empty.", fileScanner.hasNext());
        String htmlFile = fileScanner.next();
        assertEquals("File contents doesn't match.",
          TestablePreviewGenerator.DUMMY_HTML_TEMPLATE, htmlFile);
      } finally {
        zipInputStream.close();
      }
    } catch (IOException exc) {
      LOGGER.error("Exception!", exc);
      throw new RuntimeException("Failed to read zip bundle.", exc);
    }
  }

  /**
   * Tests the preview generation from zip records to a zip of previews.
   *
   * @throws TemplateNotFoundException
   * @throws PreviewGenerationException
   * @throws InvalidTemplateException
   * @throws IOException
   * @throws InterruptedException
   * @throws ResultNotFoundException
   */
//  @Test
  public void testGeneratePreviewBundleFromMultipleRecords() throws IOException, PreviewGenerationException, InvalidTemplateException, TemplateNotFoundException, InterruptedException, ResultNotFoundException {
    PreviewGenerator generator = new PreviewGenerator();
    byte[] records = null;
    ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
    ZipOutputStream zipStream = new ZipOutputStream(temporaryStream);
    ZipEntry entry = new ZipEntry("record.xml");
    zipStream.putNextEntry(entry);
    zipStream.write(RECORD.getBytes(Charset.forName("UTF-8")));
    zipStream.closeEntry();
    zipStream.finish();
    zipStream.flush();
    records = temporaryStream.toByteArray();
    zipStream.close();
    temporaryStream.close();

    boolean result = generator.generatePreviewBundleFromMultipleRecords(
      records, new PreviewTemplate(PLACEHOLDER, XSL), PLACEHOLDER,
      PLACEHOLDER);
    assertTrue("The specified resultset already exists!", result);
    while (generator.getOngoingPreviews(PLACEHOLDER).contains(PLACEHOLDER)) {
      Thread.sleep(10000);
    }

    byte[] zip = generator.getPreviewnResultAsZip(PLACEHOLDER, PLACEHOLDER);
    assertNotNull("Result is null.", zip);
    assertFalse("Byte array is zero sized.", zip.length == 0);
    ByteArrayInputStream inputStream = new ByteArrayInputStream(zip);
    ZipInputStream zipInputStream = new ZipInputStream(inputStream);
    ZipEntry zipEntry = zipInputStream.getNextEntry();
    assertEquals("Zip name doesn't match.", zipEntry.getName(),
      "preview_0.html");
    InputStreamReader reader = new InputStreamReader(zipInputStream,
      "UTF-8");
    Scanner fileScanner = new Scanner(reader);
    fileScanner.useDelimiter("\\Z");
    assertTrue("File is empty.", fileScanner.hasNext());
    String htmlFile = fileScanner.next();
    assertEquals("File contents doesn't match.", EXPECTED, htmlFile);
    reader.close();
    fileScanner.close();
    zipInputStream.close();
    inputStream.close();
    try {
      FileUtils.deleteDirectory(Paths.get(ResultDirectoryUtils.getBaseResultDirectoryPath().toString() + "/" + PLACEHOLDER).toFile());
    } catch (IOException e) {
      LOGGER.error("Exception!", e);
      Assert.fail(e.getMessage());
    }

  }
}
