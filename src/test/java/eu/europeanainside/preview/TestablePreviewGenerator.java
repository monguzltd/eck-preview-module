package eu.europeanainside.preview;

import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.entities.PreviewTemplate;

/**
 * Minimal implementation for PreviewServices class for testing.
 * @author inagy
 */
public class TestablePreviewGenerator extends PreviewGenerator {

  /**
   * Dummy HTML contents. Returned by generatePreview method.
   */
  public static final String DUMMY_HTML_TEMPLATE = "<html><body>Hello World</body></html>";

  @Override
  public String generatePreview(PreviewTemplate template, String record) throws PreviewGenerationException {
    return DUMMY_HTML_TEMPLATE;
  }
}
