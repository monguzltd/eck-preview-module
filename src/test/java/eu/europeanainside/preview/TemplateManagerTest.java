package eu.europeanainside.preview;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for the PreviewServices class.
 * @author inagy
 */
public class TemplateManagerTest {

  private static final String TEST_TEMPLATE_NAME = "something";

  /**
   * Tests template extension removal. Suffix needs to be removed.
   */
  @Test
  public void testRemoveFileSuffixWithRemoval() {
    String testName = TEST_TEMPLATE_NAME + TemplateManager.TEMPLATE_EXT_SUFFIX;
    testName = TemplateManager.removeFileSuffix(testName);
    assertEquals("Trim failed.", TEST_TEMPLATE_NAME, testName);
  }

  /**
   * Tests template extension removal. No suffix is present, so no remival
   */
  @Test
  public void testRemoveFileSuffixWithNoRemoval() {
    String testName = TemplateManager.removeFileSuffix(TEST_TEMPLATE_NAME);
    assertEquals("Unnecessarry trim.", TEST_TEMPLATE_NAME, testName);
  }
}
