package eu.europeanainside.preview.ft;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class PreviewGenerationIT {

  private static final String TEST_CONTEXT = "classpath*:test-context.xml";
  private static final String BASE_REST_PATH = "http://localhost:29999/eck-preview/";
  private static final String LIST_PATH = "templates/";
  private static final String PREVIEW_BASE_PATH = "Preview/";
  private static final String TEST_TEMPLATE_NAME = "default";
  private static final String PROVIDER_PATH = "pim/";
  private RestTemplate restTemplate;

  @Before
  public void setUp() {
    ClassPathXmlApplicationContext serverContext = new ClassPathXmlApplicationContext(TEST_CONTEXT);
    restTemplate = serverContext.getBean("restTemplate", RestTemplate.class);
  }

  @Test
  public void testGeneratePreviewWithBadTemplateAndBadRecord() throws MalformedURLException, URISyntaxException {
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME).toURI();
    String templateXML = "";
    ResponseEntity<Void> response = restTemplate.postForEntity(uri, templateXML, Void.class);

    URI previewUri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME + "/preview").toURI();
    try {
      ResponseEntity result = restTemplate.postForEntity(previewUri, "somerecord", String.class);
    } catch (HttpClientErrorException ex) {
      System.out.println("The error is: " + ex.getMessage());
    }
    restTemplate.delete(uri);
  }
}
