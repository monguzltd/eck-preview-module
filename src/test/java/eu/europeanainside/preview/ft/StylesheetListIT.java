package eu.europeanainside.preview.ft;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author gnemeth
 */
public class StylesheetListIT {

  private static final String TEST_CONTEXT = "classpath*:test-context.xml";
  private static final String BASE_REST_PATH = "http://localhost:29999/eck-preview/";
  private static final String LIST_PATH = "stylesheets/";
  private static final String TEST_STYLESHEET_NAME = "testCss";
  private static final String TEST_STYLESHEET_BODY = "";
  private static final String PREVIEW_BASE_PATH = "Preview/";
  private static final String PROVIDER_PATH = "providerName/";
  private static final Logger LOGGER = LoggerFactory.getLogger(StylesheetListIT.class);
  private RestTemplate restTemplate;

  @Before
  public void setUp() {
    ClassPathXmlApplicationContext serverContext = new ClassPathXmlApplicationContext(TEST_CONTEXT);
    restTemplate = serverContext.getBean("restTemplate", RestTemplate.class);
  }

  @Test
  public void testEmptyStylesheetList() throws RestClientException, MalformedURLException, URISyntaxException {
    LOGGER.info("test");
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH).toURI();
    LOGGER.info("uri: " + uri);
    List<LinkedHashMap> templates = restTemplate.getForObject(uri, List.class);
    LOGGER.info("The result is: " + templates);
    assertTrue(templates.isEmpty());
  }

  @Test
  public void testNotEmptyStylesheetList() throws RestClientException, MalformedURLException, URISyntaxException {
    LOGGER.info("test");
    URI stylesheetUri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_STYLESHEET_NAME).toURI();
    restTemplate.postForObject(stylesheetUri, TEST_STYLESHEET_BODY, Void.class);
    URI listUri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH).toURI();
    List<LinkedHashMap> cssFiles = restTemplate.getForObject(listUri, List.class);
    assertFalse("The result ins't empty", cssFiles.isEmpty());
    assertTrue(cssFiles.get(0).get("name").equals(TEST_STYLESHEET_NAME));
    restTemplate.delete(stylesheetUri);
  }
}
