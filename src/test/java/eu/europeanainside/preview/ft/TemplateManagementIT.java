package eu.europeanainside.preview.ft;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Template management functional tests.
 */
public class TemplateManagementIT {

  private static final String TEST_CONTEXT = "classpath*:test-context.xml";
  private static final String BASE_REST_PATH = "http://localhost:29999/eck-preview/";
  private static final String LIST_PATH = "templates/";
  private static final String TEST_TEMPLATE_NAME = "testTemplate";
  private static final String PREVIEW_BASE_PATH = "Preview/";
  private static final String PROVIDER_PATH = "providerName/";
  private static final Logger LOGGER = LoggerFactory.getLogger(TemplateManagementIT.class);
  private static final String TEST_TEMPLATE_BODY = "body";
  private RestTemplate restTemplate;

  /**
   * Setup for tests.
   */
  @Before
  public void setUp() {
    ClassPathXmlApplicationContext serverContext = new ClassPathXmlApplicationContext(TEST_CONTEXT);
    restTemplate = serverContext.getBean("restTemplate", RestTemplate.class);
  }

  /**
   * Save a template via REST.
   *
   * @throws RestClientException something is wrong with the REST template
   * @throws MalformedURLException the url is wrong
   * @throws URISyntaxException the uri syntax is invalid
   */
  @Test
  public void testSaveTemplate()
    throws RestClientException, MalformedURLException, URISyntaxException {
    LOGGER.info("test");
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME).toURI();
    restTemplate.postForObject(uri, TEST_TEMPLATE_BODY, Void.class);
    String template =
      restTemplate.getForObject(uri, String.class);
    assertEquals(TEST_TEMPLATE_BODY, template);
    restTemplate.delete(uri);
  }

  /**
   * Save, get and delete a template via REST.
   *
   * @throws RestClientException something is wrong with the REST template
   * @throws MalformedURLException the url is wrong
   * @throws URISyntaxException the uri syntax is invalid
   */
  @Test
  public void testGetTemplate()
    throws RestClientException, MalformedURLException, URISyntaxException {
    LOGGER.info("test");
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME).toURI();
    restTemplate.put(uri, TEST_TEMPLATE_BODY);
    String template =
      restTemplate.getForObject(uri, String.class);
    assertEquals(TEST_TEMPLATE_BODY, template);
    restTemplate.delete(uri);
  }

  /**
   * Save and delete a template via REST.
   *
   * @throws RestClientException something is wrong with the REST template
   * @throws MalformedURLException the url is wrong
   * @throws URISyntaxException the uri syntax is invalid
   */
  @Test
  public void testDeleteTemplate() throws RestClientException, MalformedURLException, URISyntaxException {
    LOGGER.info("test");
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME).toURI();
    restTemplate.put(uri, TEST_TEMPLATE_BODY);
    String template =
      restTemplate.getForObject(uri, String.class);
    assertEquals(TEST_TEMPLATE_BODY, template);
    restTemplate.delete(uri);
    URI listuri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH).toURI();
    List<LinkedHashMap> templates =
      restTemplate.getForObject(listuri, List.class);
    LOGGER.info("The result is: " + templates);
    assertTrue(templates.isEmpty());
  }

  /**
   * Get a non-existing template via REST.
   *
   * @throws RestClientException something is wrong with the REST template
   * @throws MalformedURLException the url is wrong
   * @throws URISyntaxException the uri syntax is invalid
   */
  @Test
  public void testGetNotExistingTemplate() throws RestClientException, MalformedURLException, URISyntaxException {
    URI uri = new URL(BASE_REST_PATH + PREVIEW_BASE_PATH + PROVIDER_PATH + LIST_PATH + TEST_TEMPLATE_NAME).toURI();
    String template = null;
    try {
      template =
        restTemplate.getForObject(uri, String.class);
    } catch (HttpServerErrorException ex) {
      LOGGER.info("The template does not exist!");
    }
    assertNull(template);
  }
}
