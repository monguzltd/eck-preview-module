package eu.europeanainside.preview;

/**
 * Constant class for preview module.
 */
public final class PreviewConstants {

  /**
   * Default private constructor for constant class.
   */
  private PreviewConstants() {
  }
  /**
   * OK status.
   */
  public static final String OK_STATUS = "OK";
  /**
   * Preview generation in progress status.
   */
  public static final String IN_PROGRESS_STATUS = "Preview generation in progress!";
}
