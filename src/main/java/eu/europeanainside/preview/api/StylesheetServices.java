package eu.europeanainside.preview.api;

import java.util.List;

import eu.europeanainside.preview.StylesheetManager;
import eu.europeanainside.preview.entities.StylesheetForTemplate;
import eu.europeanainside.preview.exception.StylesheetManipulationException;
import eu.europeanainside.preview.exception.StylesheetNotFoundException;

/**
 * @author gnemeth
 */
public class StylesheetServices {

  private StylesheetManager stylesheetManager;

  /**
   * @param stylesheetManager the stylessheetManager to set
   */
  public void setStylesheetManager(StylesheetManager stylesheetManager) {
    this.stylesheetManager = stylesheetManager;
  }

  /**
   * Listsstylesheets for a given provider.
   *
   * @param provider provider name.
   * @return available stylesheets
   */
  public List<StylesheetForTemplate> listStylesheets(String provider) {
    return stylesheetManager.listStylesheets(provider);
  }

  /**
   * Saves a stylesheet for a given name and provider.
   *
   * @param css stylesheet body
   * @param cssName stylesheet name
   * @param provider provider name
   * @throws StylesheetManipulationException if an error occurs while saving.
   */
  public void saveStylesheet(String css, String cssName, String provider) throws StylesheetManipulationException {
    stylesheetManager.saveStylesheet(css, cssName, provider);
  }

  /**
   * Returns a stylesheet by name and provider.
   *
   * @param templateName stylesheet name
   * @param provider provider name
   * @return stylesheet body
   * @throws StylesheetNotFoundException if the requested stylsheet does not exist
   * @throws StylesheetManipulationException if an error occurs while reading
   */
  public StylesheetForTemplate getSylesheet(String templateName, String provider)
    throws StylesheetNotFoundException, StylesheetManipulationException {
    return stylesheetManager.getSylesheet(templateName, provider);
  }

  /**
   * Deletes a stylehseet by name and provider.
   *
   * @param elementName stylesheet name
   * @param provider provider name
   * @throws StylesheetManipulationException if an error occurs while deleting
   */
  public void deleteStylesheet(String elementName, String provider) throws StylesheetManipulationException {
    stylesheetManager.deleteStylesheet(elementName, provider);
  }
}
