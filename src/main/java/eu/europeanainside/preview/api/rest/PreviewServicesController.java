package eu.europeanainside.preview.api.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import eu.europeanainside.preview.api.PreviewServices;
import eu.europeanainside.preview.api.StylesheetServices;
import eu.europeanainside.preview.entities.PreviewTemplate;
import eu.europeanainside.preview.entities.ShortStylesheet;
import eu.europeanainside.preview.entities.ShortTemplate;
import eu.europeanainside.preview.entities.StylesheetForTemplate;
import eu.europeanainside.preview.exception.InvalidTemplateException;
import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.exception.ResultNotFoundException;
import eu.europeanainside.preview.exception.StylesheetManipulationException;
import eu.europeanainside.preview.exception.StylesheetNotFoundException;
import eu.europeanainside.preview.exception.TemplateManipulationException;
import eu.europeanainside.preview.exception.TemplateNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Controller class providing the REST API endpoints for template and preview management.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
@Controller
@RequestMapping("Preview")
public final class PreviewServicesController {

  private static final String INTERNAL_SERVER_ERROR_RESPONSE_TEXT =
    "Internal server error. Please check server side log for details.\nError message:\n";
  private static final Logger LOGGER = LoggerFactory
    .getLogger(PreviewServicesController.class);
  private static final String TEMPLATE_NAME_KEY = "templateName";
  private static final String TEMPLATE_NAME_URL_PATTERN = "/{"
    + TEMPLATE_NAME_KEY + "}";
  private static final String PROVIDER_NAME_KEY = "provider";
  private static final String PROVIDER_NAME_URL_PATTERN = "/{"
    + PROVIDER_NAME_KEY + "}";
  private static final String SET_NUMBER_KEY = "setnumber";
  private static final String SET_NUMBER_NAME_URL_PATTERN = "/{"
    + SET_NUMBER_KEY + "}";
  private static final String STYLESHEET_NAME_KEY = "stylesheetName";
  private static final String STYLESHEET_NAME_URL_PATTERN = "/{" + STYLESHEET_NAME_KEY + "}";
  private static final String TEMPLATES_URL_PATTERN = "/templates";
  private static final String STYLESHEETS_URL_PATTERN = "/stylesheets";
  @Autowired
  private StylesheetServices stylesheetService;
  @Autowired
  private PreviewServices previewService;

  /**
   * Exception handler for template manipulation errors.
   *
   * @param exc Exception handled.
   * @return error message.
   */
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(TemplateManipulationException.class)
  @ResponseBody
  public String handleTemplateManipulationException(
    TemplateManipulationException exc) {
    LOGGER.error("There are errors handling the template.", exc);
    return INTERNAL_SERVER_ERROR_RESPONSE_TEXT + exc.getMessage();
  }

  /**
   * Provides a list of available templates by name along with links to them.
   *
   * @param providerName the provider's name
   * @return list of template names and resource links.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN + TEMPLATES_URL_PATTERN, method = RequestMethod.GET)
  @ResponseBody
  public List<ShortTemplate> listTemplates(@PathVariable(PROVIDER_NAME_KEY) String providerName) {
    List<PreviewTemplate> templates = previewService.listTemplates(providerName);
    List<ShortTemplate> hateoas = new ArrayList<ShortTemplate>();
    for (PreviewTemplate template : templates) {
      ShortTemplate ref = new ShortTemplate(template);
      Link details = linkTo(PreviewServicesController.class).slash(providerName).slash("templates").slash(
        template.getName()).withSelfRel();
      ref.add(details);
      hateoas.add(ref);
    }
    return hateoas;
  }

  /**
   * Saves a new or updates an existing template (provided in the request body).
   *
   * @param template template in XSL format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return a response entity with OK status
   * @throws TemplateManipulationException If errors occured during template save.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + TEMPLATES_URL_PATTERN
    + TEMPLATE_NAME_URL_PATTERN, method = {
    RequestMethod.POST, RequestMethod.PUT
  })
  public ResponseEntity<Void> saveTemplate(@RequestBody String template,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName)
    throws TemplateManipulationException {
    previewService.saveTemplate(template, providerName, templateName);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }

  /**
   * Returns a template by name. The template is provided in the response body in XSL format.
   *
   * @param providerName the provider's name
   * @param templateName Template name for identification.
   * @return Template body.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws TemplateManipulationException If errors occured during template retrieval.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + TEMPLATES_URL_PATTERN
    + TEMPLATE_NAME_URL_PATTERN,
    method = RequestMethod.GET)
  @ResponseBody
  public String getTemplate(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName)
    throws TemplateNotFoundException, TemplateManipulationException {
    return previewService.getTemplate(providerName, templateName).getBody();
  }

  /**
   * Deletes a template from the system.
   *
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return a response entity with OK status.
   * @throws TemplateManipulationException If errors occured during template deletion.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + TEMPLATES_URL_PATTERN
    + TEMPLATE_NAME_URL_PATTERN,
    method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteTemplate(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName)
    throws TemplateManipulationException {
    previewService.deleteTemplate(providerName, templateName);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }

  /**
   * Generates a preview from the EDM record provided in the request body using the template behind the requested.
   * resource. TODO: error handling.
   *
   * @param request the http selvet request
   * @param record record the be previewed supplied in EDM format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return preview in XHTML format.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws TemplateManipulationException If errors occured during template retrieval.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + "/single/preview" + TEMPLATE_NAME_URL_PATTERN,
    method = RequestMethod.POST, produces = {
    MediaType.APPLICATION_XHTML_XML_VALUE, MediaType.TEXT_HTML_VALUE})
  @ResponseBody
  public byte[] generatePreview(HttpServletRequest request,
    @RequestBody String record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName)
    throws PreviewGenerationException, InvalidTemplateException,
    TemplateNotFoundException, TemplateManipulationException {
    String contextPath = request.getServerName();
    return previewService.generatePreviewWithSpecialisedCssPath(record, providerName, templateName,
      contextPath);
  }

  /**
   * Generates a preview from the EDM record provided in the request body using the template behind the requested.
   * resource. TODO: error handling
   *
   * @param record record the be previewed supplied in EDM format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return preview in a zip bundle.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws TemplateManipulationException If errors occured during template retrieval.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + "/single/preview" + TEMPLATE_NAME_URL_PATTERN,
    method = RequestMethod.POST, produces = {
    MediaType.APPLICATION_OCTET_STREAM_VALUE, "application/zip"
  })
  @ResponseBody
  public byte[] generatePreviewBundle(@RequestBody String record,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName)
    throws PreviewGenerationException, InvalidTemplateException,
    TemplateNotFoundException, TemplateManipulationException {
    return previewService.generatePreviewBundle(record, providerName, templateName);
  }

  /**
   * Starts the generation of the previews for a record bundle asynchronously.
   *
   * @param zipFile the zip file with the records
   * @param providerName the provider's name
   * @param templateName the template name to use
   * @param setNumber the set number, must be unique
   * @return ACCEPTED if the generation started, CONFLICT if there was an error
   * @throws TemplateNotFoundException if the template does not exist
   * @throws TemplateManipulationException if something goes wrong while reading the template
   * @throws PreviewGenerationException if something goes wrong during preview generation
   * @throws InvalidTemplateException if the template is invalid
   * @throws IOException if an I/O error occurs
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + SET_NUMBER_NAME_URL_PATTERN
    + "/preview"
    + TEMPLATE_NAME_URL_PATTERN,
    method = RequestMethod.POST, consumes = "application/zip")
  public ResponseEntity<Void> generateZipPreviewWithTemplate(
    @RequestBody byte[] zipFile,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(TEMPLATE_NAME_KEY) String templateName,
    @PathVariable(SET_NUMBER_KEY) String setNumber)
    throws TemplateNotFoundException, TemplateManipulationException,
    PreviewGenerationException, InvalidTemplateException, IOException {

    if (previewService.generatePreviewForRecordListWithTemplate(zipFile, templateName,
      providerName, setNumber)) {
      return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    } else {
      return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    }
  }

  /**
   * Returns the result of the asynchronous preview generation if the result is ready.
   *
   * @param providerName the provider's name
   * @param setNumber the set name
   * @return the record previews as a zip file
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + SET_NUMBER_NAME_URL_PATTERN + "/result",
    method = RequestMethod.GET,
    produces = "application/zip")
  @ResponseBody
  public ResponseEntity<byte[]> getZipPreviewResult(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(SET_NUMBER_KEY) String setNumber) {
    byte[] result = null;
    try {
      result = previewService.getZipPreviewResult(providerName, setNumber);
    } catch (PreviewGenerationException ex) {
      return new ResponseEntity<byte[]>(HttpStatus.PROCESSING);
    } catch (ResultNotFoundException ex) {
      return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<byte[]>(result, HttpStatus.OK);
  }

  /**
   * Returns the sets which are in progress status by provider name.
   *
   * @param providerName the provider's name
   * @return the list of sets in progress
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN + "/sets", method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public List<String> getPreviewsInProcessingStatus(@PathVariable(PROVIDER_NAME_KEY) String providerName) throws ResultNotFoundException {
    return previewService.getPreviewsInProcessingStatus(providerName);
  }

  /**
   * Provice the list of all available css, and link to them.
   *
   * @param providerName the provider's name
   * @return the collected available css and the resource links
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN + STYLESHEETS_URL_PATTERN, method = RequestMethod.GET)
  @ResponseBody
  public List<ShortStylesheet> listStylesheets(@PathVariable(PROVIDER_NAME_KEY) String providerName) {
    List<StylesheetForTemplate> listStylesheets = stylesheetService.listStylesheets(providerName);
    List<ShortStylesheet> hateoas = new ArrayList<ShortStylesheet>();
    for (StylesheetForTemplate stylesheetForTemplate : listStylesheets) {
      ShortStylesheet stylesheet = new ShortStylesheet(stylesheetForTemplate);
      Link details = linkTo(PreviewServicesController.class).slash(stylesheetForTemplate.getName()).withSelfRel();
      stylesheet.add(details);
      hateoas.add(stylesheet);
    }
    return hateoas;
  }

  /**
   * Saves a new or updates an existing css file (provided in the request body).
   *
   * @param css the css body
   * @param providerName the provider's name
   * @param stylesheetName stylesheet name for identification.
   * @throws StylesheetManipulationException If errors occured during css save.
   * @return Rsponse to server, status ok
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + STYLESHEETS_URL_PATTERN
    + STYLESHEET_NAME_URL_PATTERN,
    method = {RequestMethod.POST, RequestMethod.PUT
  })
  public ResponseEntity<Void> saveStylesheet(@RequestBody String css,
    @PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(STYLESHEET_NAME_KEY) String stylesheetName)
    throws StylesheetManipulationException {
    stylesheetService.saveStylesheet(css, stylesheetName, providerName);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }

  /**
   * Returns a stylesheet by name. The stylesheet is provided in the response body in css format.
   *
   * @param providerName the provider's name
   * @param stylesheetName stylesheet name for identification.
   * @return the stylesheet.
   * @throws StylesheetNotFoundException If the stylesheet does not exist.
   * @throws StylesheetManipulationException If errors occured during template retrieval.
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + STYLESHEETS_URL_PATTERN
    + STYLESHEET_NAME_URL_PATTERN,
    method = RequestMethod.GET)
  @ResponseBody
  public String getStylesheet(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(STYLESHEET_NAME_KEY) String stylesheetName)
    throws StylesheetNotFoundException, StylesheetManipulationException {
    return stylesheetService.getSylesheet(stylesheetName, providerName).getBody();
  }

  /**
   * Deletes a template from the system.
   *
   * @param providerName the provider's name
   * @param stylesheetName template name for identification.
   * @throws StylesheetManipulationException If errors occured during stylesheet deletion.
   * @return the http response: OK
   */
  @RequestMapping(value = PROVIDER_NAME_URL_PATTERN
    + STYLESHEETS_URL_PATTERN
    + STYLESHEET_NAME_URL_PATTERN,
    method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteStylesheet(@PathVariable(PROVIDER_NAME_KEY) String providerName,
    @PathVariable(STYLESHEET_NAME_KEY) String stylesheetName)
    throws StylesheetManipulationException {
    stylesheetService.deleteStylesheet(stylesheetName, providerName);
    return new ResponseEntity<Void>(HttpStatus.OK);
  }
}
