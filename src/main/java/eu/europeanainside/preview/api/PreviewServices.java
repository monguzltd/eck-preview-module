package eu.europeanainside.preview.api;

import java.io.IOException;
import java.util.List;

import eu.europeanainside.preview.PreviewGenerator;
import eu.europeanainside.preview.TemplateManager;
import eu.europeanainside.preview.entities.PreviewTemplate;
import eu.europeanainside.preview.exception.InvalidTemplateException;
import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.exception.ResultNotFoundException;
import eu.europeanainside.preview.exception.TemplateManipulationException;
import eu.europeanainside.preview.exception.TemplateNotFoundException;

/**
 * Service provider class for exposing preview services to the REST API and internal usages.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public class PreviewServices {

  private PreviewGenerator previewGenerator;
  private TemplateManager templateManager;

  /**
   * Provides a list of available templates by name along with links to them.
   *
   * @param providerName the provider's name
   * @return list of template names and resource links.
   */
  public List<PreviewTemplate> listTemplates(String providerName) {
    return templateManager.listTemplates(providerName);
  }

  /**
   * Saves a new or updates an existing template (provided in the request body).
   *
   * @param templateBody template in XSL format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @throws TemplateManipulationException If errors occured during template save.
   */
  public void saveTemplate(String templateBody, String providerName,
    String templateName) throws TemplateManipulationException {
    templateManager.saveTemplate(templateBody, providerName, templateName);
  }

  /**
   * Returns a template by name. The template is provided in the response body in XSL format.
   *
   * @param providerName the provider's name
   * @param templateName Template name for identification.
   * @return Template body.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws TemplateManipulationException If an error occurs during template handling.
   */
  public PreviewTemplate getTemplate(String providerName, String templateName)
    throws TemplateNotFoundException, TemplateManipulationException {
    return templateManager.getTemplate(providerName, templateName);
  }

  /**
   * Deletes a template from the system.
   *
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @throws TemplateManipulationException If errors occured during template deletion.
   */
  public void deleteTemplate(String providerName, String templateName)
    throws TemplateManipulationException {
    templateManager.deleteTemplate(providerName, templateName);
  }

  /**
   * Generates a preview from the EDM record provided in the request body using the template behind the requested.
   *
   * @param record record the be previewed supplied in EDM format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return preview in XHTML format.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws TemplateManipulationException If an error occurs during template loading.
   */
  public String generatePreview(String record, String providerName,
    String templateName) throws PreviewGenerationException,
    InvalidTemplateException, TemplateNotFoundException,
    TemplateManipulationException {
    return previewGenerator.generatePreview(
      getTemplate(providerName, templateName), record);
  }

  /**
   * Generates a preview from the EDM record provided in the request body using the template behind the requested.
   *
   * @param record record the be previewed supplied in EDM format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @param contextPath the host name, where will got the stylesheets
   * @return preview in XHTML format.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws TemplateManipulationException If an error occurs during template loading.
   */
  public byte[] generatePreviewWithSpecialisedCssPath(String record,
    String providerName, String templateName, String contextPath)
    throws PreviewGenerationException, InvalidTemplateException,
    TemplateNotFoundException, TemplateManipulationException {
    return previewGenerator.generatePreviewWithSpecialisedCssPath(
      getTemplate(providerName, templateName), record, contextPath);
  }

  /**
   * Generates a preview from the EDM record provided in the request body using the template behind the requested.
   *
   * @param record record the be previewed supplied in EDM format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @return preview in a zip bundle.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws TemplateManipulationException If an error occurs during template loading.
   */
  public byte[] generatePreviewBundle(String record, String providerName,
    String templateName) throws PreviewGenerationException,
    InvalidTemplateException, TemplateNotFoundException,
    TemplateManipulationException {

    return previewGenerator.generatePreviewBundle(record,
      getTemplate(providerName, templateName));
  }

  /**
   * @param previewGenerator the previewGenerator to set
   */
  public void setPreviewGenerator(PreviewGenerator previewGenerator) {
    this.previewGenerator = previewGenerator;
  }

  /**
   * @param templateManager the templateManager to set
   */
  public void setTemplateManager(TemplateManager templateManager) {
    this.templateManager = templateManager;
  }

  /**
   * Starts the batch preview generation.
   *
   * @param recordBundle the records in a zip file.
   * @param templateName the template's name
   * @param providerName the provider's name
   * @param setNumber the set's name
   * @return true, if the preview generation started, false if conflict occured.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws TemplateManipulationException If the template exists, but is invalid.
   * @throws PreviewGenerationException If the specified set already exists.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws IOException If there was an I/O error
   */
  public boolean generatePreviewForRecordListWithTemplate(byte[] recordBundle,
    String templateName, String providerName, String setNumber)
    throws TemplateNotFoundException, TemplateManipulationException,
    PreviewGenerationException, InvalidTemplateException, IOException {

    PreviewTemplate template = templateManager.getTemplate(providerName, templateName);
    return previewGenerator.generatePreviewBundleFromMultipleRecords(
      recordBundle, template, providerName, setNumber);
  }

  /**
   * Returns the result of a specified set of batch records by the provider name.
   *
   * @param providerName the provider's name
   * @param setNumber the set name
   * @return the result as a zip
   * @throws PreviewGenerationException if the result generation is in progress.
   * @throws ResultNotFoundException If the result does not exist.
   */
  public byte[] getZipPreviewResult(String providerName, String setNumber)
    throws PreviewGenerationException, ResultNotFoundException {
    return previewGenerator.getPreviewnResultAsZip(providerName, setNumber);
  }

  /**
   * Returns the set names that are in progress.
   *
   * @param providerName the provider's name
   * @return a list of sets in progress by provider name
   */
  public List<String> getPreviewsInProcessingStatus(String providerName) throws ResultNotFoundException {
    return previewGenerator.getOngoingPreviews(providerName);
  }
}
