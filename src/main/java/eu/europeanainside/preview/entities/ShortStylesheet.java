package eu.europeanainside.preview.entities;

import org.springframework.hateoas.ResourceSupport;

/**
 *
 * @author gnemeth
 */
public class ShortStylesheet extends ResourceSupport {

  private String name;

  /**
   * Default constructor.
   */
  public ShortStylesheet() {
    super();
  }

  /**
   * Constructor.
   *
   * @param base the {@link SylesheetForTemplate} for set the name variable
   */
  public ShortStylesheet(StylesheetForTemplate base) {
    super();
    this.name = base.getName();
  }

  /**
   * @return the name of stylesheet
   */
  public String getName() {
    return name;
  }

  /**
   * @param name to set the nam ef the stylesheet
   */
  public void setName(String name) {
    this.name = name;
  }
}
