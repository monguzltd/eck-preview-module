package eu.europeanainside.preview.entities;

import org.springframework.hateoas.ResourceSupport;

/**
 * HATEOAS-conformant resource representation for preview templates.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class ShortTemplate extends ResourceSupport {

  private String name;

  /**
   * Constructor. Creates a HATEOAS-compatible instance from the internal representation.
   *
   * @param base the internal instance
   */
  public ShortTemplate(PreviewTemplate base) {
    super();
    this.name = base.getName();
  }

  /**
   * Default constructor.
   */
  public ShortTemplate() {
    super();
  }

  /**
   * Setter.
   *
   * @param name template name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }
}
