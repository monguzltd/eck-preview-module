package eu.europeanainside.preview.entities;

/**
 *
 * @author Gábor
 */
public class StylesheetForTemplate {

  private String name;
  private String body;

  /**
   * Create an instance.
   *
   * @param name the stylesheet name
   * @param body the content
   */
  public StylesheetForTemplate(String name, String body) {
    this.name = name;
    this.body = body;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the body
   */
  public String getBody() {
    return body;
  }

  /**
   * @param body the body to set
   */
  public void setBody(String body) {
    this.body = body;
  }
}
