package eu.europeanainside.preview.entities;

/**
 * Entity class representing preview templates.
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class PreviewTemplate {

  private final String name;
  private final String body;

  /**
   * Constructor.
   * @param name template name
   * @param body template body text
   */
  public PreviewTemplate(String name, String body) {
    this.name = name;
    this.body = body;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the body
   */
  public String getBody() {
    return body;
  }

}
