package eu.europeanainside.preview;

import eu.europeanainside.preview.exception.ResultNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

import eu.europeanainside.preview.utils.ResultDirectoryUtils;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holds the states of batch previews.
 */
public class BatchPreviewStateCache {

  private final Map<String, Map<String, String>> cache;
  private static final Logger LOGGER = LoggerFactory.getLogger(BatchPreviewStateCache.class);

  /**
   * Default constructor that initialize the cache map from the container directories.
   */
  public BatchPreviewStateCache() {
    this.cache = Collections
      .synchronizedMap(new HashMap<String, Map<String, String>>());

    Path resultDirPath = ResultDirectoryUtils.getBaseResultDirectoryPath();
    try {
      DirectoryStream<Path> dirStream = Files.newDirectoryStream(resultDirPath);
      for (Path providerPath : dirStream) {
        DirectoryStream<Path> subdirStream = Files.newDirectoryStream(providerPath);
        Map<String, String> set = new HashMap<String, String>();
        for (Path setPath : subdirStream) {
          set.put(FilenameUtils.removeExtension(setPath.toFile().getName()), PreviewConstants.OK_STATUS);
        }
        cache.put(providerPath.toFile().getName(), set);
      }
    } catch (IOException ex) {
      LOGGER.error("Error while reading the directory!", ex);
    }
  }

  /**
   * The manager put the set number into the cache when the preview generation starts.
   *
   * @param setNumber set's number
   * @param providerName name of the provider
   */
  public void putIdentifier(String setNumber, String providerName) {
    Map<String, String> status = new HashMap<String, String>();
    status.put(setNumber, PreviewConstants.IN_PROGRESS_STATUS);
    cache.put(providerName, status);
  }

  /**
   * Selects the preview set by the given set number and provider name, and updates the status.
   *
   * @param setNumber set's number
   * @param state new status
   * @param providerName name of the provider
   * @throws IllegalArgumentException If the preview set or the provider doesn't exist
   */
  public void updatePreviewStatus(String setNumber, String state,
    String providerName) throws ResultNotFoundException {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        status.put(setNumber, state);
      } else {
        throw new ResultNotFoundException(
          "The provider doesn't contain the following id: " + setNumber);
      }
    } else {
      throw new ResultNotFoundException(
        "The cache doesn't contain the following provider: " + providerName);
    }
  }

  /**
   * Gets the preview set's status.
   *
   * @param setNumber set's number
   * @param providerName name of the provider
   * @throws ResultNotFoundException If the preview set or the provider doesn't exist
   * @return the state of the preview generation
   */
  public String getPreviewState(String setNumber, String providerName)
    throws ResultNotFoundException {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        return status.get(setNumber);
      } else {
        throw new ResultNotFoundException(
          "The provider doesn't contain the following id: " + setNumber);
      }
    } else {
      throw new ResultNotFoundException(
        "The cache doesn't contain the following provider: " + providerName);
    }
  }

  /**
   * Gets the list of preview sets, which are still in progress.
   *
   * @param providerName name of the provider
   * @return list of sets
   * @throws IllegalArgumentException If the provider doesn't exist
   */
  public List<String> getOngoingPreviews(String providerName)
    throws ResultNotFoundException {
    List<String> result = new ArrayList<String>();

    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      for (Map.Entry<String, String> entry : status.entrySet()) {
        if (entry.getValue().equals(PreviewConstants.IN_PROGRESS_STATUS)) {
          result.add(entry.getKey());
        }
      }
    } else {
      throw new ResultNotFoundException(
        "The cache doesn't contain the following provider: " + providerName);
    }
    return result;
  }

  /**
   * Checks that the requested set exists or not.
   *
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return true if the requested set exists, else false
   * @throws IllegalArgumentException
   */
  public boolean doesPreviewSetExist(String setNumber, String providerName) {
    if (cache.containsKey(providerName)) {
      Map<String, String> status = cache.get(providerName);
      if (status.containsKey(setNumber)) {
        return true;
      }
    }
    return false;
  }
}
