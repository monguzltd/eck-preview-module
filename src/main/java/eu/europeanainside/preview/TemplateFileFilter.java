package eu.europeanainside.preview;

import java.io.File;
import java.io.FileFilter;

/**
 * Accepts template files ending with .template. Used by template file listing.
 * @author István Nagy <inagy@monguz.hu>
 */
public class TemplateFileFilter implements FileFilter {

  @Override
  public boolean accept(File arg0) {
    String fileName = arg0.getName();
    if (fileName == null) {
      return false;
    } else {
      fileName = fileName.toUpperCase();
    }

    return fileName.endsWith(TemplateManager.TEMPLATE_EXT_SUFFIX.toUpperCase());
  }
}
