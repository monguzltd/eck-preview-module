package eu.europeanainside.preview;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.preview.entities.PreviewTemplate;
import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.exception.ResultNotFoundException;
import eu.europeanainside.preview.utils.ResultDirectoryUtils;

/**
 * This class is responsible for the batch preview generation.
 */
public class BatchPreviewManager implements Callable<Void> {

  private static final Logger LOGGER = LoggerFactory
    .getLogger(BatchPreviewManager.class);
  private PreviewTemplate template;
  private List<String> recordList;
  private PreviewGenerator generator;
  private BatchPreviewStateCache cache;
  private String providerName;
  private String setName;

  @Override
  public Void call() throws PreviewGenerationException, ResultNotFoundException {
    int count = 0;
    for (String record : recordList) {
      LOGGER.debug("Called preview bundle on template: " + template.getName()
        + " with record size " + record.length());
      String previewHtml = generator.generatePreview(template, record);
      File file = new File(ResultDirectoryUtils.getResultDirectoryPath(
        providerName, setName) + "/preview_" + count++ + ".html");

      try {
        FileWriter writer = new FileWriter(file);
        try {
          writer.write(previewHtml);
          writer.flush();
        } finally {
          writer.close();
        }
      } catch (Exception ex) {
        LOGGER.error("Error while writing result file!", ex);
      }
    }

    cache.updatePreviewStatus(setName, PreviewConstants.OK_STATUS, providerName);

    return null;
  }

  /**
   * Getter.
   *
   * @return template
   */
  public PreviewTemplate getTemplate() {
    return template;
  }

  /**
   * Setter.
   *
   * @param template template
   */
  public void setTemplate(PreviewTemplate template) {
    this.template = template;
  }

  /**
   * Getter.
   *
   * @return record list
   */
  public List<String> getRecordList() {
    return recordList;
  }

  /**
   * Setter.
   *
   * @param recordList the record list
   */
  public void setRecordList(List<String> recordList) {
    this.recordList = recordList;
  }

  /**
   * Getter.
   *
   * @return the preview generator
   */
  public PreviewGenerator getGenerator() {
    return generator;
  }

  /**
   * Setter.
   *
   * @param generator preview generator
   */
  public void setGenerator(PreviewGenerator generator) {
    this.generator = generator;
  }

  /**
   * Getter.
   *
   * @return preview state holder cache
   */
  public BatchPreviewStateCache getCache() {
    return cache;
  }

  /**
   * Setter.
   *
   * @param cache preview state holder cache
   */
  public void setCache(BatchPreviewStateCache cache) {
    this.cache = cache;
  }

  /**
   * Getter.
   *
   * @return provider name
   */
  public String getProviderName() {
    return providerName;
  }

  /**
   * Setter.
   *
   * @param providerName provider name
   */
  public void setProviderName(String providerName) {
    this.providerName = providerName;
  }

  /**
   * Getter.
   *
   * @return setname
   */
  public String getSetName() {
    return setName;
  }

  /**
   * Setter.
   *
   * @param setName setname
   */
  public void setSetName(String setName) {
    this.setName = setName;
  }
}
