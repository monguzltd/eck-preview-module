package eu.europeanainside.preview.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;

/**
 * Utility class for directories.
 */
public final class ResultDirectoryUtils {

  private static final String ECK_PREVIEW_RESULT_HOME_PATH = ".eck/preview-result/";
  private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(ResultDirectoryUtils.class);
  private static final String EXCEPTION = "Exception!";

  /**
   * Default constructor for utility class.
   */
  private ResultDirectoryUtils() {
  }

  /**
   * Gets the results base directory path.
   *
   * @return path of directory
   */
  public static Path getBaseResultDirectoryPath() {
    String home = System.getProperty("user.home");
    Path userHome = Paths.get(home);

    Path baseResultDirectoryPath = userHome
      .resolve(ECK_PREVIEW_RESULT_HOME_PATH);
    if (!baseResultDirectoryPath.toFile().exists()) {
      try {
        Files.createDirectories(baseResultDirectoryPath);
      } catch (IOException e) {
        LOGGER.error(EXCEPTION, e);
      }
    }
    return baseResultDirectoryPath;
  }

  /**
   * Gets the provider based result directory path.
   *
   * @param providerName name of the provider
   * @param setName the set name
   * @return path of directory
   */
  public static Path getResultDirectoryPath(String providerName, String setName) {
    String home = System.getProperty("user.home");
    Path userHome = Paths.get(home);

    Path resultDirectoryPath = userHome.resolve(ECK_PREVIEW_RESULT_HOME_PATH
      + providerName + "/" + setName);
    try {
      Files.createDirectories(resultDirectoryPath);
    } catch (IOException e) {
      LOGGER.error(EXCEPTION, e);
    }
    return resultDirectoryPath;
  }

  /**
   * Gets the requested result file in zip format.
   *
   * @param files record XML files
   * @return result in zip file
   */
  public static byte[] getResultAsZip(List<Path> files) {
    byte[] result = null;

    ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
    try {
      ZipOutputStream zipStream = new ZipOutputStream(temporaryStream);
      try {
        for (Path record : files) {
          ZipEntry previewEntry = new ZipEntry(record.getFileName().toString());
          zipStream.putNextEntry(previewEntry);
          String content = new Scanner(record.toFile()).useDelimiter("\\Z")
            .next();
          zipStream.write(content.getBytes(Charset.forName("UTF-8")));
          zipStream.closeEntry();
        }
        zipStream.finish();
        zipStream.flush();

        temporaryStream.flush();
        result = temporaryStream.toByteArray();
        temporaryStream.close();
      } finally {
        zipStream.close();
      }
    } catch (IOException exc) {
      LOGGER.error(EXCEPTION, exc);
    }
    return result;
  }

  /**
   * Gets the result file from the container directory.
   *
   * @param setNumber set's number
   * @param providerName name of the provider
   * @return result file
   */
  public static List<Path> getResultFiles(String setNumber, String providerName) {
    List<Path> result = new ArrayList<Path>();

    Path resultDirPath = getResultDirectoryPath(providerName, setNumber);
    try {
      DirectoryStream<Path> stream = Files.newDirectoryStream(resultDirPath);
      try {
        for (Path entry : stream) {
          result.add(entry);
        }
      } finally {
        stream.close();
      }
    } catch (DirectoryIteratorException ex) {
      LOGGER.error(EXCEPTION, ex);
    } catch (IOException ex) {
      LOGGER.error(EXCEPTION, ex);
    }

    return result;
  }
}
