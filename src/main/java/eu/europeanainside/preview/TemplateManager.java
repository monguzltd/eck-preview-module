package eu.europeanainside.preview;

import eu.europeanainside.preview.exception.TemplateNotFoundException;
import eu.europeanainside.preview.exception.TemplateManipulationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.preview.entities.PreviewTemplate;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

/**
 * Class responsible for template management, storage and retrieval.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class TemplateManager {

  private static final Logger LOGGER = LoggerFactory
    .getLogger(TemplateManager.class);
  /**
   * Template file name suffix.
   */
  protected static final String TEMPLATE_EXT_SUFFIX = ".template"; //NOPMD needed for other classes.
  private static final String ECK_PREVIEW_TEMPLATES_HOME_PATH = ".eck/preview-templates/";
  private static final String SYSTEM_PROPERTY_USER_HOME = "user.home";
  private static final String FILE_EOF_REGEX = "\\Z";
  private URI templateDirUri;

  /**
   * Initializes the bean.
   */
  public void init() {
    String home = System.getProperty(SYSTEM_PROPERTY_USER_HOME);
    URI userHome = new File(home).toURI();

    templateDirUri = userHome.resolve(ECK_PREVIEW_TEMPLATES_HOME_PATH);
    File templateDir = new File(templateDirUri);
    if (!templateDir.exists()) {
      LOGGER
        .debug("ECK preview template directory doesn't exist. Creating one.");
      templateDir.mkdirs();
    }
  }

  /**
   * Provides a list of available templates by name along with links to them.
   *
   * @param providerName the provider's name
   * @return list of template names and resource links.
   */
  public List<PreviewTemplate> listTemplates(String providerName) {
    List<PreviewTemplate> templates = new ArrayList<PreviewTemplate>();
    try {
      DirectoryStream<Path> dirStream =
        Files.newDirectoryStream(Paths.get(templateDirUri.resolve(providerName)),
        "*" + TemplateManager.TEMPLATE_EXT_SUFFIX);
      for (Path templateFilePath : dirStream) {
        try {
          templates.add(readTemplate(templateFilePath.toFile()));
        } catch (TemplateNotFoundException ex) {
          LOGGER.warn("File went missing during reading: "
            + templateFilePath.getFileName());
        } catch (TemplateManipulationException ex) {
          LOGGER
            .error("Error retrieving template: " + templateFilePath.getFileName(), ex);
        }
      }
    } catch (Exception exc) {
      LOGGER.error("Something wrong happened!", exc);
    }
    return templates;
  }

  /**
   * Saves a new or updates an existing template (provided in the request body).
   *
   * @param template template in XSL format.
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @throws TemplateManipulationException If an I/O error occurs during template save.
   */
  public void saveTemplate(String template, String providerName,
    String templateName) throws TemplateManipulationException {
    if ("default".equals(providerName)) {
      LOGGER.warn("The default templates are not allowed to be updated from the client side!");
      return;
    }
    try {
      File templateFile = getTemplateFile(providerName, templateName, true);
      FileWriter templateFileWriter = new FileWriter(templateFile, false);
      try {
        templateFileWriter.write(template);
        templateFileWriter.flush();
      } finally {
        templateFileWriter.close();
      }
    } catch (IOException ex) {
      throw new TemplateManipulationException(ex);
    }
  }

  /**
   * Returns a template by name. The template is provided in the response body in XSL format.
   *
   * @param providerName the provider's name
   * @param templateName Template name for identification.
   * @return Template body.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws TemplateManipulationException If an error occurs during template handling.
   */
  public PreviewTemplate getTemplate(String providerName, String templateName)
    throws TemplateNotFoundException, TemplateManipulationException {
    File templateFile = getTemplateFile(providerName, templateName, false);
    if (!templateFile.exists()) {
      throw new TemplateNotFoundException();
    }
    return readTemplate(templateFile);
  }

  /**
   * Deletes a template from the system.
   *
   * @param providerName the provider's name
   * @param templateName template name for identification.
   * @throws TemplateManipulationException If an error occurs during template deletion.
   */
  public void deleteTemplate(String providerName, String templateName)
    throws TemplateManipulationException {
    if ("default".equals(providerName)) {
      LOGGER.warn("The default templates are not allowed to be deleted from the client side!");
      return;
    }
    File templateFile = getTemplateFile(providerName, templateName, true);
    if (templateFile.exists() && !templateFile.delete()) {
      throw new TemplateManipulationException("Failed to delete template.");
    }
  }

  /**
   * Removes a template extension from a template filename.
   *
   * @param name The name to remove extension.
   * @return The name without the extension.
   */
  protected static String removeFileSuffix(String name) {
    if (name.toUpperCase().endsWith(TEMPLATE_EXT_SUFFIX.toUpperCase())) {
      name = name.substring(0, name.length() - TEMPLATE_EXT_SUFFIX.length());
    }
    return name;
  }

  /**
   * Returns a Java File object for the given template name.
   *
   * @param providerName the provider's name
   * @param templateName The name of the template.
   * @param isManipulation true if save or delete, false in other cases
   * @return The template as a Java file object.
   */
  private File getTemplateFile(String providerName, String templateName,
    boolean isManipulation) {
    if (!templateName.endsWith(TEMPLATE_EXT_SUFFIX)) {
      templateName = templateName + TEMPLATE_EXT_SUFFIX;
    }

    URI templateFileUri = templateDirUri.resolve(providerName + "/"
      + templateName);
    File templateFile = new File(templateFileUri);
    if (isManipulation) {
      try {
        if (!templateFile.getParentFile().exists()) {
          templateFile.getParentFile().mkdirs();
        }
        templateFile.createNewFile();
      } catch (IOException ex) {
        LOGGER.error("Template file could not be created!");
      }
    } else if (!isManipulation && !templateFile.exists()) {
      templateFileUri = templateDirUri.resolve("default/" + templateName);
      templateFile = new File(templateFileUri);
      if (!templateFile.exists()) {
        return null;
      }
    }
    return templateFile;
  }

  /**
   * Reads a template from a Java File object.
   *
   * @param templateFile The File object to read template from.
   * @return The readed template.
   * @throws TemplateNotFoundException If the Template can't be opened.
   * @throws TemplateManipulationException If an error occurs during template handling.
   */
  private PreviewTemplate readTemplate(File templateFile)
    throws TemplateNotFoundException, TemplateManipulationException {
    try {
      Scanner templateScanner = new Scanner(templateFile);
      templateScanner.useDelimiter(FILE_EOF_REGEX);
      try {
        if (templateScanner.hasNext()) {
          String name = removeFileSuffix(templateFile.getName());
          return new PreviewTemplate(name, templateScanner.next()); // NOPMD
          // It's fine
          // here.
        } else {
          throw new TemplateManipulationException(
            "Empty template body in template " + templateFile.getName());
        }
      } catch (Exception exc) {
        throw new TemplateManipulationException(exc);
      } finally {
        templateScanner.close();
      }
    } catch (FileNotFoundException e) {
      throw new TemplateNotFoundException(e);
    }
  }
}
