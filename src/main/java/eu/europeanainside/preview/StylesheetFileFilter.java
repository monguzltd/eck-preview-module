package eu.europeanainside.preview;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author gnemeth
 */
public class StylesheetFileFilter implements FileFilter {

  @Override
  public boolean accept(File pathname) {
    String fileName = pathname.getName();
    if (fileName == null) {
      return false;
    } else {
      fileName = fileName.toUpperCase();
    }
    return fileName.endsWith(StylesheetManager.STYLESHEET_EXT_SUFFIX.toUpperCase());
  }
}
