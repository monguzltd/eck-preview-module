package eu.europeanainside.preview.exception;

/**
 * Exception class handling errors occurring while handling template files..
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class TemplateManipulationException extends AbstractManipulationException {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public TemplateManipulationException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public TemplateManipulationException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public TemplateManipulationException(Throwable cause) {
    super(cause);
  }
}
