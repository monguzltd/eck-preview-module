package eu.europeanainside.preview.exception;

/**
 * Exception class handling errors during template generation.
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class PreviewGenerationException extends Exception {

  private static final long serialVersionUID = 1L;

  /** Default constructor. */
  public PreviewGenerationException() {
    super();
  }

  /**
   * Constructor.
   * @param message Error message.
   */
  public PreviewGenerationException(String message) {
    super(message);
  }

  /**
   * Constructor.
   * @param cause Underlying exception.
   */
  public PreviewGenerationException(Throwable cause) {
    super(cause);
  }

  /**
   * Constructor.
   * @param message Error message.
   * @param cause Underlying exception
   */
  public PreviewGenerationException(String message, Throwable cause) {
    super(message, cause);
  }
}
