package eu.europeanainside.preview.exception;

/**
 * Exception class handling errors occurring while handling manupulating the
 * files..
 *
 * @author gnemeth
 */
public abstract class AbstractManipulationException extends Exception {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public AbstractManipulationException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public AbstractManipulationException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public AbstractManipulationException(Throwable cause) {
    super(cause);
  }
}
