package eu.europeanainside.preview.exception;

/**
 * Exception class handling errors occurring while handling (specified)
 * stylesheets files..
 *
 * @author gnemet
 */
public class StylesheetManipulationException extends AbstractManipulationException {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public StylesheetManipulationException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public StylesheetManipulationException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public StylesheetManipulationException(Throwable cause) {
    super(cause);
  }
}
