package eu.europeanainside.preview.exception;

/**
 * Exception for that case when the preview processing result cannot be found.
 */
public class ResultNotFoundException extends Exception {

  /**
   * Default constructor.
   */
  public ResultNotFoundException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message error message
   * @param cause error cause
   */
  public ResultNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor.
   *
   * @param message error message
   */
  public ResultNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause error cause
   */
  public ResultNotFoundException(Throwable cause) {
    super(cause);
  }
}
