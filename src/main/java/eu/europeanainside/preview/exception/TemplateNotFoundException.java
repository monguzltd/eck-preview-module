package eu.europeanainside.preview.exception;

/**
 * Exception class representing a case where a nonexistent template was
 * referenced.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class TemplateNotFoundException extends AbstractNoFoundException {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public TemplateNotFoundException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public TemplateNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public TemplateNotFoundException(Throwable cause) {
    super(cause);
  }
}
