package eu.europeanainside.preview.exception;

/**
 *
 * @author gnemeth
 */
public class AbstractNoFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public AbstractNoFoundException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public AbstractNoFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public AbstractNoFoundException(Throwable cause) {
    super(cause);
  }
}
