package eu.europeanainside.preview.exception;

/**
 * Exception class representing a case where a nonexistent stylesheet was
 * referenced.
 *
 * @author gnemeth
 */
public class StylesheetNotFoundException extends AbstractNoFoundException {

  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public StylesheetNotFoundException() {
    super();
  }

  /**
   * Constructor.
   *
   * @param message Error message.
   */
  public StylesheetNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor.
   *
   * @param cause Underlying exception.
   */
  public StylesheetNotFoundException(Throwable cause) {
    super(cause);
  }
}
