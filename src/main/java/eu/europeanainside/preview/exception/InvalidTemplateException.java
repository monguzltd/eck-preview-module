package eu.europeanainside.preview.exception;

/**
 * Exception class handling errors resulting from invalid templates.
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public final class InvalidTemplateException extends Exception {

  private static final long serialVersionUID = 1L;

  /** Default constructor. */
  public InvalidTemplateException() {
    super();
  }

  /**
   * Constructor.
   * @param message Error message.
   */
  public InvalidTemplateException(String message) {
    super(message);
  }

  /**
   * Constructor.
   * @param cause Underlying exception.
   */
  public InvalidTemplateException(Throwable cause) {
    super(cause);
  }
}
