package eu.europeanainside.preview;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.xml.transform.StringResult;
import org.springframework.xml.transform.StringSource;
import org.xml.sax.SAXException;

import eu.europeanainside.preview.entities.PreviewTemplate;
import eu.europeanainside.preview.exception.InvalidTemplateException;
import eu.europeanainside.preview.exception.PreviewGenerationException;
import eu.europeanainside.preview.exception.ResultNotFoundException;
import eu.europeanainside.preview.exception.TemplateNotFoundException;
import eu.europeanainside.preview.utils.ResultDirectoryUtils;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Class responsible for preview generation using the given record and template.
 *
 * @author Ágoston Berger <aberger@monguz.hu>
 */
public class PreviewGenerator {

  private static final Logger LOGGER = LoggerFactory
    .getLogger(PreviewGenerator.class);
  /**
   * The preview file's name in the generated bundle.
   */
  protected static final String PREVIEW_HTML_NAME = "preview.html";
  /**
   * The default CSS file name.
   */
  protected static final String DEFAULT_CSS_NAME = "theme.css";
  private String templateXsdPath;
  private static final BatchPreviewStateCache PREVIEW_STATE_CACHE = new BatchPreviewStateCache();
  private final ExecutorService executor = Executors.newFixedThreadPool(10);
  private static final String UTF8_BOM = "\uFEFF";

  /**
   * Validates a supplied template. Throws InvalidTemplateException if there are any errors.
   *
   * @param template Template to be validated.
   * @throws InvalidTemplateException If the template is invalid or an error occurs.
   */
  public void validateTemplate(PreviewTemplate template)
    throws InvalidTemplateException {
    try {
      LOGGER.debug("Called validate on template: " + template.getName());
      SchemaFactory factory = SchemaFactory
        .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = factory.newSchema(new URL(templateXsdPath));
      Source templateSource = new StringSource(template.getBody());
      Validator validator = schema.newValidator();
      validator.validate(templateSource);
    } catch (SAXException ex) {
      LOGGER.error("", ex);
      throw new InvalidTemplateException(ex);
    } catch (IOException ex) {
      LOGGER.error("", ex);
      throw new InvalidTemplateException(ex);
    } catch (IllegalArgumentException ex) {
      LOGGER.error("", ex);
      throw new InvalidTemplateException(ex);
    }
  }

  /**
   * Generates a HTML preview using the given record and template.
   *
   * @param template Template instance.
   * @param record Record to generate a preview from.
   * @return The generated preview as HTML.
   * @throws PreviewGenerationException If an error occurs during preview generation.
   */
  public String generatePreview(PreviewTemplate template, String record)
    throws PreviewGenerationException {
    try {
      LOGGER.debug("Called preview on template: " + template.getName()
        + " with record size " + record.length());
      LOGGER.trace("record:" + record);
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StringSource(
        template.getBody()));
      StringResult result = new StringResult();
      transformer.transform(new StringSource(record), result);
      LOGGER.trace("result: " + result.toString());
      return result.toString();
    } catch (TransformerConfigurationException ex) {
      LOGGER.error("", ex);
      throw new PreviewGenerationException(ex);
    } catch (TransformerException ex) {
      LOGGER.error("", ex);
      throw new PreviewGenerationException(ex);
    }
  }

  /**
   * Generates a HTML preview using the given record and template.
   *
   * @param template Template instance.
   * @param record Record to generate a preview from.
   * @param contextPath the url
   * @return The generated preview as HTML.
   * @throws PreviewGenerationException If an error occurs during preview generation.
   */
  public byte[] generatePreviewWithSpecialisedCssPath(
    PreviewTemplate template, String record, String contextPath)
    throws PreviewGenerationException {
    try {
      LOGGER
        .debug("Called preview on template: " + template.getName()
        + " with record size " + record.length() + " context: "
        + contextPath);
      if (record.startsWith(UTF8_BOM)) {
        record = record.replaceFirst(UTF8_BOM, "");
      }
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StringSource(
        template.getBody()));
      transformer.setParameter("hostName", contextPath);
      transformer.setParameter("stylesheetName", DEFAULT_CSS_NAME);
      transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
      transformer.setParameter("encoding", "UTF-8");
      ByteArrayOutputStream output = new ByteArrayOutputStream();
      StreamResult result = new StreamResult(new OutputStreamWriter(output));
      ByteArrayInputStream input;
//      String returnValue = null;
      try {
        input = new ByteArrayInputStream(record.getBytes("UTF-8"));
        transformer.transform(new StreamSource(new InputStreamReader(input, "UTF-8")), result);
//        returnValue = new String(output.toByteArray(), "UTF-8");
      } catch (UnsupportedEncodingException ex) {
        LOGGER.error("Unsupported encoding!", ex);
      }
      return output.toByteArray();

//      return returnValue;
    } catch (TransformerConfigurationException ex) {
      LOGGER.error("", ex);
      throw new PreviewGenerationException(ex);
    } catch (TransformerException ex) {
      LOGGER.error("", ex);
      throw new PreviewGenerationException(ex);
    }
  }

  /**
   * Generates a preview bundled as ZIP.
   *
   * @param record record the be previewed supplied in EDM format.
   * @param template Template to use with the preview.
   * @return preview in a zip bundle.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   */
  public byte[] generatePreviewBundle(String record, PreviewTemplate template)
    throws PreviewGenerationException, InvalidTemplateException,
    TemplateNotFoundException {

    LOGGER.debug("Called preview bundle on template: " + template.getName()
      + " with record size " + record.length());

    byte[] result = null;

    ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
    try {
      ZipOutputStream zipStream = new ZipOutputStream(temporaryStream);
      try {
        ZipEntry previewEntry = new ZipEntry(PREVIEW_HTML_NAME);
        zipStream.putNextEntry(previewEntry);
        String previewHtml = generatePreview(template, record);
        zipStream.write(previewHtml.getBytes(Charset.forName("UTF-8")));

        zipStream.closeEntry();
        zipStream.finish();
        zipStream.flush();

        result = temporaryStream.toByteArray();
      } finally {
        zipStream.close();
      }
    } catch (IOException exc) {
      LOGGER.error("", exc);
      throw new PreviewGenerationException(
        "Failed to create preview ZIP stream contents.", exc);
    }

    return result;
  }

  /**
   * Starts the asynchronuos processing of a bundle of records.
   *
   * @param recordBundle the records in a zip
   * @param template the template
   * @param providerName the provider's name
   * @param setNumber the set name
   * @return true if the processing started, false if the set already exists for the provider
   * @throws PreviewGenerationException If a system error occurred during preview generation.
   * @throws InvalidTemplateException If the template exists, but is invalid.
   * @throws TemplateNotFoundException If the template does not exist.
   * @throws IOException If something happened during file operations.
   */
  public boolean generatePreviewBundleFromMultipleRecords(byte[] recordBundle,
    PreviewTemplate template, String providerName, String setNumber)
    throws PreviewGenerationException, InvalidTemplateException,
    TemplateNotFoundException, IOException {

    List<String> recordList = new ArrayList<String>();
    byte[] buffer = new byte[1024];

    try {
      ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(
        recordBundle));
      try {
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
          int len;
          ByteArrayOutputStream bos = new ByteArrayOutputStream();
          while ((len = zis.read(buffer)) > 0) {
            bos.write(buffer, 0, len);
          }
          String xmlContent = bos.toString("UTF-8");
          recordList.add(xmlContent);
          zipEntry = zis.getNextEntry();
          bos.close();
        }
        zis.closeEntry();
      } finally {
        zis.close();
      }
    } catch (Exception exc) {
      LOGGER.error("", exc);
      throw new PreviewGenerationException(
        "Failed to create preview ZIP stream contents.", exc);
    }

    if (PREVIEW_STATE_CACHE.doesPreviewSetExist(setNumber, providerName)) {
      return false;
    } else {
      PREVIEW_STATE_CACHE.putIdentifier(setNumber, providerName);
      BatchPreviewManager manager = new BatchPreviewManager();
      manager.setGenerator(this);
      manager.setRecordList(recordList);
      manager.setTemplate(template);
      manager.setProviderName(providerName);
      manager.setCache(PREVIEW_STATE_CACHE);
      manager.setSetName(setNumber);
      FutureTask<Void> future = new FutureTask<Void>(manager);

      executor.execute(future);
      return true;
    }
  }

  /**
   * Returns the previews that are in 'in progress' state.
   *
   * @param providerName the provider's name
   * @return a list of the set names of the previews in progress
   */
  public List<String> getOngoingPreviews(String providerName) throws ResultNotFoundException {
    return PREVIEW_STATE_CACHE.getOngoingPreviews(providerName);
  }

  /**
   * Returns the result of a preview as a zip file.
   *
   * @param providerName the provider's name
   * @param setNumber the set name
   * @return the zip with the generated previews
   * @throws PreviewGenerationException If the preview generation is in progress state.
   * @throws ResultNotFoundException If the result does not exist.
   */
  public byte[] getPreviewnResultAsZip(String providerName, String setNumber)
    throws PreviewGenerationException, ResultNotFoundException {
    String status = PREVIEW_STATE_CACHE.getPreviewState(setNumber, providerName);
    if (status.equals(PreviewConstants.OK_STATUS)) {
      return ResultDirectoryUtils.getResultAsZip(ResultDirectoryUtils
        .getResultFiles(setNumber, providerName));
    } else if (status.equals(PreviewConstants.IN_PROGRESS_STATUS)) {
      throw new PreviewGenerationException("Preview generation in progress!");
    } else {
      throw new ResultNotFoundException();
    }
  }

  /**
   * @param templateXsdPath the templateXsdPath to set
   */
  public void setTemplateXsdPath(String templateXsdPath) {
    this.templateXsdPath = templateXsdPath;
  }
}
