package eu.europeanainside.preview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.europeanainside.preview.entities.StylesheetForTemplate;
import eu.europeanainside.preview.exception.StylesheetManipulationException;
import eu.europeanainside.preview.exception.StylesheetNotFoundException;

/**
 * @author gnemeth
 */
public class StylesheetManager {

  private static final Logger LOGGER = LoggerFactory.getLogger(StylesheetManager.class);
  /**
   * Stylesheet file name suffix.
   */
  protected static final String STYLESHEET_EXT_SUFFIX = ".css";
  private static final String ECK_PREVIEW_STYLESHEETS_HOME_PATH = ".eck/preview-templates/";
  private static final String SYSTEM_PROPERTY_USER_HOME = "user.home";
  private static final String FILE_EOF_REGEX = "\\Z";
  private URI baseDirUri;

  /**
   * Initializes the bean.
   */
  public void init() {
    String home = System.getProperty(SYSTEM_PROPERTY_USER_HOME);
    URI userHome = new File(home).toURI();

    baseDirUri = userHome.resolve(ECK_PREVIEW_STYLESHEETS_HOME_PATH);
    File templateDir = new File(baseDirUri);
    if (!templateDir.exists()) {
      LOGGER.debug("ECK preview template directory doesn't exist. Creating one.");
      templateDir.mkdirs();
    }
  }

  /**
   * List of the availabel stylesheets.
   *
   * @param provider provider name.
   * @return collect of the stylesheetst width {@link StylesheetForTemplate}
   */
  public List<StylesheetForTemplate> listStylesheets(String provider) {
    List<StylesheetForTemplate> csses = new ArrayList<StylesheetForTemplate>();
    File cssFileDir = new File(resolveProviderPath(provider));
    File[] cssFiles = cssFileDir.listFiles(new StylesheetFileFilter());
    for (File cssFile : cssFiles) {
      try {
        csses.add(readCss(cssFile));
      } catch (StylesheetNotFoundException ex) {
        LOGGER.warn("File went missing during reading: " + cssFile.getName());
      } catch (StylesheetManipulationException ex) {
        LOGGER.error("Error retrieving stylesheets: " + cssFile.getName(), ex);
      }
    }
    return csses;
  }

  /**
   * Save or modidfy an existing stylesheet.
   *
   * @param css the new or existing stylesheet
   * @param cssName the name of the new or existing stylesheet
   * @param provider provider name.
   * @throws StylesheetManipulationException if has some file manipulating exception
   */
  public void saveStylesheet(String css, String cssName, String provider) throws StylesheetManipulationException {
    try {
      File cssFile = getCssFile(cssName, provider, true);
      FileWriter cssFileWriter = new FileWriter(cssFile, false);
      try {
        cssFileWriter.write(css);
        cssFileWriter.flush();
      } finally {
        cssFileWriter.close();
      }
    } catch (IOException ex) {
      throw new StylesheetManipulationException(ex);
    }
  }

  /**
   * Returns a {@link File} object for the given stylesheet name.
   *
   * @param cssName The name of the stylesheet.
   * @param provider provider name.
   * @param isManipulation true if save or delese, in other cases false
   * @return The stylesheet as a Java file object.
   */
  private File getCssFile(String cssName, String provider, boolean isManipulation) {
    String fileNameWithSuffix;
    if (cssName.endsWith(STYLESHEET_EXT_SUFFIX)) {
      fileNameWithSuffix = cssName;
    } else {
      fileNameWithSuffix = cssName + STYLESHEET_EXT_SUFFIX;
    }
    URI providerDirUri = resolveProviderPath(provider);
    URI cssFileUri = providerDirUri.resolve(fileNameWithSuffix);
    File cssFile = new File(cssFileUri);
    if (!cssFile.exists() && !isManipulation) {
      cssFileUri = resolveProviderPath("default").resolve(fileNameWithSuffix);
      cssFile = new File(cssFileUri);
    }
    return cssFile;
  }

  /**
   * @param cssName name of the css
   * @param provider provider name.
   * @return the {@link StylesheetForTemplate} by file name if existing
   * @throws StylesheetNotFoundException if the stylesheet not foiund by name
   * @throws StylesheetManipulationException if has some exception in the file operations
   */
  public StylesheetForTemplate getSylesheet(String cssName, String provider)
    throws StylesheetNotFoundException, StylesheetManipulationException {
    File stylesheetFile = getCssFile(cssName, provider, false);
    if (!stylesheetFile.exists()) {
      throw new StylesheetNotFoundException();
    }
    return readCss(stylesheetFile);
  }

  /**
   * Delete an existing stylesheet.
   *
   * @param elementName the name of the css what want to delete
   * @param provider provider name.
   * @throws StylesheetManipulationException if has some exception in the file operations
   */
  public void deleteStylesheet(String elementName, String provider) throws StylesheetManipulationException {
    File stylesheetFile = getCssFile(elementName, provider, true);
    if (stylesheetFile.exists()) {
      if (!stylesheetFile.delete()) { // NOPMD, this looks like better
        throw new StylesheetManipulationException("Failed to delete CSS.");
      }
    }
  }

  /**
   * Reads a stylesheet from a Java {@link File} object.
   *
   * @param cssFile The {@link File} object to read template from.
   * @return The readed css.
   * @throws StylesheetNotFoundException If the css can't be opened.
   * @throws StylesheetManipulationException If an error occurs during css handling.
   */
  private StylesheetForTemplate readCss(File cssFile)
    throws StylesheetNotFoundException, StylesheetManipulationException {
    Scanner cssScanner = null;
    try {
      try {
        cssScanner = new Scanner(cssFile);
      } catch (FileNotFoundException exc) {
        throw new StylesheetNotFoundException(exc);
      }

      cssScanner.useDelimiter(FILE_EOF_REGEX);
      try {
        if (cssScanner.hasNext()) {
          String name = removeFileSuffix(cssFile.getName());
          return new StylesheetForTemplate(name, cssScanner.next());
        } else {
          throw new StylesheetManipulationException("Empty css body in file " + cssFile.getName());
        }
      } catch (Exception exc) {
        throw new StylesheetManipulationException(exc);
      }
    } finally {
      if (cssScanner != null) {
        cssScanner.close();
      }
    }
  }

  /**
   * Removes a stylesheet extension from a stylesheet filename.
   *
   * @param name The name to remove extension.
   * @return The name without the extension.
   */
  protected static String removeFileSuffix(String name) {
    String returnValue;
    if (name.toUpperCase(Locale.ENGLISH).endsWith(STYLESHEET_EXT_SUFFIX.toUpperCase(Locale.ENGLISH))) {
      returnValue = name.substring(0, name.length() - STYLESHEET_EXT_SUFFIX.length());
    } else {
      returnValue = name;
    }
    return returnValue;
  }

  /**
   * Ensures the final path (per provider) exists.
   *
   * @param provider provider name
   * @return provider path URI
   */
  private URI resolveProviderPath(String provider) {
    URI providerPath = baseDirUri.resolve(provider + "/");
    File providerDir = new File(providerPath);
    if (!providerDir.exists()) {
      LOGGER.debug("Provider directory doesn't exist. Creating one.");
      providerDir.mkdirs();
    }
    return providerPath;
  }
}
